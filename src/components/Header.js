import React, { Component } from 'react';
import { Dimensions, View, Text, TextInput, StyleSheet, Platform, StatusBar, TouchableOpacity, Image, ColorPropType } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { colorPrimary, main, mainblue, white } from "../common/Colors";
import { connect } from "react-redux";
import { fontFamily_Regular, fontFamily_Bold } from '../common/Constant';
import fontsConst from '../common/fontsConst';
// import {  } from '../common/Constant';

let extra = "";

const actions = [
    {
        text: "Accessibility",
        //   icon: require("./images/ic_accessibility_white.png"),
        name: "bt_accessibility",
        position: 2
    },
    {
        text: "Language",
        //   icon: require("./images/ic_language_white.png"),
        name: "bt_language",
        position: 1
    },
    {
        text: "Location",
        //   icon: require("./images/ic_room_white.png"),
        name: "bt_room",
        position: 3
    },
    {
        text: "Video",
        //   icon: require("./images/ic_videocam_white.png"),
        name: "bt_videocam",
        position: 4
    }
];

export default class Headerr extends Component {
    //   static navigationOptions = {
    //     header: null
    //   } 
    constructor(props) {
        super(props);
        this.state = {
            Left: this.props.left,
            Center: this.props.center,
            Right: this.props.right,
            Icons: this.props.Icons,
            iconname: this.props.iconname,
            update: this.props.update

        }
    }

    openSearch = () => {
        this.props.navigation.navigate('SearchPage')
    }
    handleViewRef = ref => this.view = ref;

    bounce = () => this.view.bounce(800).then(endState => console.log(endState.finished ? 'bounce finished' : 'bounce cancelled'));

    render() {
        const {title ,goBack } = this.props;
        return (
            <View style={[styles.header, { backgroundColor: "transparent", height: Platform.OS === "ios" ? 40 + hp("4%") : 20, }]}>
                <View style={[styles.container, { backgroundColor: "transparent" }]}>
                    <View style={{ left: wp("4%"), padding: 8, padding: 5, position: "absolute", alignItems: "center", justifyContent: "center" }}>

                        {goBack && 
                            <View style={{ left: wp("2%"), padding: 4, position: "absolute", alignItems: "center", justifyContent: "center" }}>
                                <TouchableOpacity onPress={()=> this.props.goBackHandler()}>
                                    <Text style={{ color: "black", fontWeight: "500", fontSize: 22 }}>{title}</Text>

                                </TouchableOpacity>
                            </View>
                        }
                   </View>
                </View>

            </View>
        );
    }
}



const styles = StyleSheet.create({
    header: {
        width: wp("100%"),
        // height: Platform.OS === "ios" ? props.extra + hp("4%") : props.extra,
        backgroundColor: white,
        alignItems: 'center',
        justifyContent: "center",
    },
    container: {
        width: wp("100%"),
        height: 56,
        backgroundColor: colorPrimary,
        position: "absolute",
        bottom: 0,
        alignItems: 'center',
        justifyContent: "center",
        flexDirection: "row",
    },
    iconStyle: {
        marginLeft: wp("2%"),
        color: white,
    },
    title: {
        fontSize: 20,
        color: white,
        width: "60%",
        textAlign: "center"

    }
});
