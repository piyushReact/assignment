import React, {Component} from 'react';
import {
  Modal,
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  BackHandler,
  Dimensions,
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class ModalMap extends Component {
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.onClose();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }
 markerDragHandler=async(e)=>{
     const userLat = e.nativeEvent.coordinate.latitude
     const userLong = e.nativeEvent.coordinate.longitude;
     const region = {
       userLat: e.nativeEvent.coordinate.latitude,
       userLong : e.nativeEvent.coordinate.longitude
     };
        await this.setState({
            region: e.nativeEvent.coordinate, user_lat: e.nativeEvent.coordinate.latitude,
            user_lng: e.nativeEvent.coordinate.longitude })
        this.props.updateLocation(region);
        }


  render() {
    const {props} = this;
    return (
      <Modal visible={true} animationType={'slide'} requestOnClose={true}>
        <SafeAreaView style={styles.wrapper}>
          <View style={styles.container}>
            <View
              style={[
                styles.header,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  padding: 15,
                },
              ]}>
              <Text style={[styles.headerText]}>
                {props.title || 'Select Location'}
              </Text>
              {/* <Icon {...cross} onPress={props.onClose} /> */}
              <TouchableOpacity onPress={props.onClose} style={styles.done}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
            <MapView
              //   provider={MapView.PROVIDER_GOOGLE}
              // onRegionChange={this.onRegionChange}
              showsMyLocationButton={true}
              showsUserLocation={true}
              style={{
                height: height,
                width: width,
                alignSelf: 'center',
              }}
              //   provider={'google'}
              region={{
                latitude: props.region.latitude,
                longitude: props.region.longitude,
                latitudeDelta: 0.2922,
                longitudeDelta: 0.2421,
              }}>
              <Marker
                draggable
                coordinate={props.region}
                resizeMode={'cover'}
                onDragEnd={e => this.markerDragHandler(e)}>
                {/* <Image source={require('../../../../assets/navigation.png')} style={{ height: 50, width : 50 }}/> */}
              </Marker>
            </MapView>
          </View>
        </SafeAreaView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },

  container: {
    padding: 10,
    
  },
  checked: {
    color: '#68DBD6',
  },
  text: {
    fontSize: 17,
  },
  header: {
    backgroundColor: '#F4F8F8',
  },
  headerText: {
    fontSize: 18,
    textTransform: 'uppercase',
  },
  item: {
    paddingHorizontal: 15,
    borderBottomColor: '#F4F8F8',
    borderBottomWidth: 1,
    minHeight: 55,
  },
  done: {
    padding: 10,
  },
});

export default ModalMap;
