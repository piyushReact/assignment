import ImagePicker from 'react-native-image-crop-picker';

export default showImagePicker = async (index, circle, type, successPicker) => {
  let width = 500,
    height = 500;
  if (type === 'profile') {
    width = 240;
    height = 240;
  } else if (type === 'background') {
    width = 828;
    height = 640;
  }
  let obj = {
    width: width,
    height: height,
    cropping: true,
    cropperCircleOverlay: circle,
    compressImageMaxWidth: 828,
    compressImageMaxHeight: 640,
    compressImageQuality: 0.5,
    compressVideoPreset: 'MediumQuality',
    includeBase64: true,
    includeExif: true,
  };
  if (index == 1) {
    // setTimeout(() => {
      ImagePicker.openPicker(obj)
        .then(image => {
          let imageData = {
            uri: image.path,
            width: image.width,
            height: image.height,
            // name: image.filename === undefined ? "image" : image.filename,
            name: 'imgageName.jpeg',
            //  type: image.mime
          };
          console.log('check this ==> image data ==> abc ', imageData);
          // let base64 = `data:${image.mime};base64,`+ image.data;
          if (image.path) {
            successPicker(image.path, imageData);
          }
        })
        .catch(response => {
          console.log('Gallery error : ', response);
        });
    // }, 300);
  } else if (index == 2) {
    setTimeout(() => {
      ImagePicker.openCamera(obj)
        .then(image => {
          console.log('Utils Image : ', image);
          let imageData = {
            uri: image.path,
            width: image.width,
            height: image.height,
            name: 'imgageName.jpeg',
            type: image.mime,
          };
          if (image.path) {
            successPicker(image.path, imageData);
          }
        })
        .catch(response => {
          console.log('Camera error : ', response);
        });
    }, 300);
  }
};
