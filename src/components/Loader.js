import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator
} from 'react-native';
import { colorPrimary , mainblue } from '../common/Colors';
import { PrintLog } from './PrintLog';
import { connect, dispatch } from "react-redux"; 

const Loader = props => {
  const {
    loading,
    ...attributes
  } = props;
  return (
    
    <Modal
    
      transparent={true}
      animationType={'none'}
      // visible={loading}
      visible={props.state.stateStatus}
      onRequestClose={() => PrintLog('close modal')}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator size={'large'}
            color={"grey"}
            animating={loading} />
        </View>
      </View>
    </Modal>
  )  
}
const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column', 
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: colorPrimary,
    height: 100,
    width: 100,
    borderRadius: 10, 
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});

const mapStateToProps = state => {
  console.log("Login_dispatch  dispatch 2 ==> ",state)
  return {
      state : {
        stateStatus : state.LoaderReducer.loading, 
        // loading : state.TermandConditions.loading
    }
  }
};
const mapDispatchToProps = dispatch => (
  {
  Login_dispatch: payload => {
      dispatch(Login_dispatch(payload))
  },
  START_LOADING: payload => {
      dispatch(START_LOADING())
  },
  STOP_LOADING: () => {
      dispatch(STOP_LOADING())
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(Loader);



