import {View, Text, Platform, Alert, Linking, BackHandler} from 'react-native';

export const AsyncAlert = async (Permision, text) =>
  new Promise((resolve, reject) => {
    Alert.alert(
      Permision,
      text,
      [
        {
          text: 'Cancel',
          onPress: () => {
            reject(false);
          },
        },
        {
          text: 'OK',
          onPress: () => {
            resolve(true);
          },
        },
      ],
      {cancelable: false},
    );
  });
