import React, {Component} from 'react';
import {Platform, Linking, BackHandler} from 'react-native';
import {
  check,
  PERMISSIONS,
  RESULTS,
  request,
  openSettings,
} from 'react-native-permissions';
import {AsyncAlert} from './PermissionAlert';

export default checkContectsPermissions = (
  iOS_Permission,
  Android_Permission,
  Permission_AlertHeading,
  Permission_AlertTextiOS,
  Permission_AlertTextAndroid,
) =>
  new Promise((resolve, reject) => {
    Platform.OS == 'ios'
      ? check(iOS_Permission)
          .then(async result => {
            console.log('Permission Status  ::  ' + result);
            switch (result) {
              case RESULTS.UNAVAILABLE:
                break;
              case RESULTS.DENIED:
                let response = await getPermissions(
                  iOS_Permission,
                  Android_Permission,
                );
                response ? resolve(true) : reject(false);
                break;
              case RESULTS.GRANTED:
                resolve(true);
                break;
              case RESULTS.BLOCKED:
                let response1 = await AsyncAlert(
                  Permission_AlertHeading,
                  Permission_AlertTextiOS,
                )
                  .then(res => {
                    navigateToAppSettings();
                  })
                  .catch(err => {
                    reject(false);
                  });
                return response1;
                break;
            }
          })
          .catch(error => {
            console.log(error);
          })
      : check(Android_Permission)
          .then(async result => {
            console.log('Permission Status  ::  ' + result);
            switch (result) {
              case RESULTS.UNAVAILABLE:
                break;
              case RESULTS.DENIED:
                let response = await getPermissions(
                  iOS_Permission,
                  Android_Permission,
                );
                response ? resolve(true) : reject(false);
                break;
              case RESULTS.GRANTED:
                resolve(true);
                break;
              case RESULTS.BLOCKED:
                let response1 = await AsyncAlert(
                  Permission_AlertHeading,
                  Permission_AlertTextAndroid,
                )
                  .then(res => {
                    navigateToAppSettings();
                  })
                  .catch(err => {
                    reject(false);
                  });
                return response1;
                break;
            }
          })
          .catch(error => {
            console.log(error);
            reject(false);
          });
  });

async function getPermissions(iOS_Permission, Android_Permission) {
  if (Platform.OS == 'ios') {
    return request(iOS_Permission)
      .then(result => {
        if (result == 'granted') {
          return true;
        } //result== granted or blocked
        else {
          return false;
        }
      })
      .catch(error => {
        return false;
      });
  } else {
    return request(Android_Permission)
      .then(result => {
        if (result == 'granted') {
          return true;
        } //result== granted or blocked
        else {
          return false;
        }
      })
      .catch(error => {
        return false;
      });
  }
}

function navigateToAppSettings() {
  if (Platform.OS === 'ios') {
    Linking.canOpenURL('app-settings:')
      .then(supported => {
        console.log(`Settings url33 works`);
        Linking.openURL('app-settings:');
      })
      .catch(error => {
        console.log(`An error has occured: ${error}`);
      });
  } else {
    BackHandler.exitApp();
    openSettings();
  }
}
