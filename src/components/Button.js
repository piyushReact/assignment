import React from 'react'
import {TouchableOpacity, Text} from 'react-native';

export const buttonComponent = (props) => {
    return(
        <TouchableOpacity onPress={()=> props.SubmitHandler()} style={{ height : 30, width : 75 ,borderWidth : 1, justifyContent : "center", alignItems : "center", borderStyle : "dashed" }}>
            <Text>{props.title}</Text>
        </TouchableOpacity> 
    )
}

export default buttonComponent;