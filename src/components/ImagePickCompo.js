import React, {Component} from 'react';
import {TextInput, View,Image, Text, TouchableOpacity,Modal} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ShowImagePicker from './imagePicker';
// import Modal from 'react-native-modal';
// import base64 from 'react-native-base64';

class ImagePickerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisibleModal: false,
    };
  }

  callModal = () => {
    this.setState({isVisibleModal: true});
  };

  getPhoto = type => {
    this.setState({isVisibleModal: false});
    setTimeout(() => {
      type == 'Camera'
        ? ShowImagePicker(2, true, 'profile', this.onGet_image)
        : ShowImagePicker(1, true, 'profile', this.onGet_image);
    }, 500);
  };
  onGet_image = async (uri, image) => {
    const {input, meta, ...inputProps} = this.props;
    console.log('success uri ==> ', uri);
    console.log('success uri inputs==> ', input);

    console.log('success uri image ==> ', image);
    // const param = {
    //   uri : image.uri,
    //   width: 300,
    //   height: 300,
    //   name: 'imgageName.jpeg',
    // }; 
    input.onChange(image.uri); 

  };

  modal_view = () => {
    // const { Cancel, O_Camera, C_F_Gallery } = strings;
    return (
      <TouchableOpacity
        onPress={() => this.setState({isVisibleModal: false})}
        style={{width: wp('100%'), height: hp('100%'), alignSelf: 'center'}}>
        <View
          style={{
            width: wp('100%'),
            height: hp('28%'),  
            position: 'absolute',
            bottom: 0,
            flexDirection: 'column',
            // backgroundColor: 'transparent',
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
          }}>
          <TouchableOpacity
            onPress={() => this.getPhoto('Camera')}
            style={{
              width: '100%',
              height: '34%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: wp('5%')}}>Open Camera</Text>
          </TouchableOpacity>
          <View
            style={{
              width: wp('90%'),
              height: hp('0.1%'),
              alignSelf: 'center',
              backgroundColor: '#E7E7E7',
            }}></View>

          <TouchableOpacity
            onPress={() => this.getPhoto('Gallery')}
            style={{
              width: wp('100%'),
              height: hp('5%'),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: wp('5%')}}>Choose From Gallery</Text>
          </TouchableOpacity>
          <View
            style={{
              width: wp('90%'),
              height: hp('0.1%'),
              alignSelf: 'center',
              backgroundColor: '#E7E7E7',
            }}></View>

          <TouchableOpacity
            onPress={() => this.setState({isVisibleModal: false})}
            style={{
              width: wp('100%'),
              height: hp('10%'),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: wp('5%'), color: '#103040'}}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };
  render() {
    const {input, meta, ...inputProps} = this.props;
    console.log('DocumentView ==> ', input.value);
    return (
      <TouchableOpacity
        style={{
          height: 100,
          width: 100,
          borderRadius: 50,
          //   backgroundColor: 'red',
        }}
        onPress={
          () => this.setState({isVisibleModal: !this.state.isVisibleModal})
          // this.props.selectOneFile()
        }>
        {input.value ? (
          <Image
            style={{
              height: 100,
              width: 100,
              borderRadius: 50,
            }}
            source={{
              uri: input.value,
            }}
          />
        ) : (
          <Image
            style={{
              height: 100,
              width: 100,
              borderRadius: 50,
              borderWidth: 2,
            }}
            source={{
              uri: 'https://indepacific.com/img/user.png',
            }}
          />
        )}
        {this.state.isVisibleModal && (
          <Modal
            animationIn="fadeIn"
            animationOut="fadeOut"
            isVisible={this.state.isVisibleModal}>
            {this.modal_view()}
          </Modal>
        )}
      </TouchableOpacity>
    );
  }
}

export default ImagePickerComponent;
