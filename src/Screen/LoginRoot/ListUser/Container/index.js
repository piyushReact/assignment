import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Keyboard,
  Text,
  Platform,
  SafeAreaView,
} from 'react-native';
import UserListComponent from './UserListComp';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class UserList extends Component {

   async componentDidMount(){
     this.props.START_LOADING()
       await this.props.getUserList()
    }

  submitHandler(data) {
    this.props.addDataDispatch(data);
  }

  render() {
    console.log('need data ==> ', this.props.userList);
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1}}>
          <SafeAreaView />
          <UserListComponent
            userList={this.props.userList}
            deleteHandler={id => this.props.deleteHandlerDespatch(id)}
          />
        </View>
        <TouchableOpacity onPress={()=> this.props.navigation.navigate("SignIn")}
          style={{flex: 1 / 10, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{ fontSize : 18 }}>ADD USER</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fieldView: {height: height / 20, width: width / 1.2, borderBottomWidth: 2},
});

export default UserList;
