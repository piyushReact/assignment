import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,Alert,
  Keyboard,
  FlatList,
  Text,
  Platform,
  SafeAreaView,
  Image,
} from 'react-native';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import ButtonComp from '../../../../components/Button';
import * as NavigationService from '../../../../navigation/NavigationService';
import {showPermissionAlert} from '../../../../common//Utils';

class UserListComp extends Component {
  constructor(props) {
    super(props);
  }

  askPersmission = id => {
    Alert.alert(
      'User List',
      'Do you want to delete the User',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => this.gotoDelete(id)},
      ],
      {
        cancelable: false,
      },
    );
  };
  gotoDelete=(id)=>{
    this.props.deleteHandler(id);
  }

  itemComponent = item => {
    console.log('title => @piyush', item);
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={() =>
          NavigationService.navigate('UserDetails', {detailsData: item})
        }>
        <View
          style={{
            flex: 1 / 2,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {/* JSON.stringify(obj) */}
          {JSON.stringify(item.avatar) != JSON.stringify({}) ? (
            <Image
              source={{uri: item.avatar}}
              style={{
                height: width / 6,
                width: width / 6,
                borderRadius: width / 6,
              }}
            />
          ) : (
            <Image
              source={{uri: 'https://indepacific.com/img/user.png'}}
              style={{
                height: width / 6,
                width: width / 6,
                borderRadius: width / 6,
              }}
            />
          )}
        </View>
        <View
          style={{
            flex: 1 / 2,
            justifyContent: 'space-evenly',
            alignItems: 'center',
          }}>
          <View style={{flex: 1 / 2, justifyContent: 'center'}}>
            <Text style={styles.title}>{item.name}</Text>
          </View>
          <View style={{flex: 1 / 2, justifyContent: 'center'}}>
            <ButtonComp
              title={'DELETE'}
              SubmitHandler={() => this.askPersmission(item.id)}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _listEmptyComponent() {
    return (
      <View
        style={{
          height: height,
          width: width,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 18}}>No Record Found</Text>
      </View>
    );
  }
  render() {
    const {userList} = this.props;
    console.log(userList.userList.data);
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          style={{flex: 1}}
          data={userList.userList.data}
          ListEmptyComponent={this._listEmptyComponent}
          renderItem={
            item => {
              return this.itemComponent(item.item);
            }
            // Item item={item} data={userList.userList.data}
            // )
          }
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  fieldView: {height: height / 20, width: width / 1.2, borderBottomWidth: 2},
  container: {flex: 1},
  item: {
    marginTop: 10,
    height: height / 8,
    width: width,
    flexBasis: 10,
    flex : 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    elevation: 50,
    backgroundColor: 'white',
    shadowColor: '#607293',
    shadowRadius: 20,
    paddingBottom: 10,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 0,
      height: 20,
    },
    borderColor: 'white',
  },
});

export default UserListComp;
