import React, {Component} from 'react';
import {STOP_LOADING, START_LOADING, getUserList, deleteHandlerDespatch} from '../../../action';
import {connect, dispatch} from 'react-redux';
import UserListComponent from './Container';

const mapStateToProps = state => {
  return {
    userList: {
      userList: state.userList,
    },
  };
};
const mapDispatchToProps = dispatch => ({
  getUserList: payload => {
    dispatch(getUserList(payload));
  },
  deleteHandlerDespatch: payload => {
    dispatch(deleteHandlerDespatch(payload));
  },
  START_LOADING: () => {
    dispatch(START_LOADING());
  },
  STOP_LOADING: () => {
    dispatch(STOP_LOADING());
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(UserListComponent);
