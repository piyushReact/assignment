import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Keyboard,
  Text,
  Platform,ScrollView,
  SafeAreaView,
} from 'react-native';
import * as NavigationService from '../../../../navigation/NavigationService';
import UserDetails from './UserDetails';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class UserDescriptionCompo extends Component {
    constructor(props){
        super(props)
        this.state={
            userDetails : undefined
        }
    }

  componentDidMount(){
      const data = this.props.navigation.getParam('detailsData');
      this.setState({userDetails : data}); 
  }
  render() { 
      const {userDetails} = this.state;
    return (
      <View style={{flex: 1}}>
        <SafeAreaView /> 
        <UserDetails dataRecord={userDetails} />
        <SafeAreaView />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fieldView: {height: height / 20, width: width / 1.2, borderBottomWidth: 2},
});

export default UserDescriptionCompo;
