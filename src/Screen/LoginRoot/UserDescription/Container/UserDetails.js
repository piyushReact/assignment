import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
  Keyboard,
  Text,
  Platform,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import * as NavigationService from '../../../../navigation/NavigationService';
import UserDetails from './UserDetails';
import MapView, {Marker} from 'react-native-maps';
import Header from '../../../../components/Header';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class UserDetailsComp extends Component {
  render() {  
      const {dataRecord} = this.props;
      console.log('checkong foinal ==> ', dataRecord);
    return (
      <ScrollView contentContainerStyle={{flex: 1, flexDirection: 'column'}}>
        <Header
          goBack
          title={'BACK'}
          goBackHandler={() => NavigationService.goBack()}
        />
        {/* NavigationService.goBack() */}
        <View
          style={{
            flex: 1,
          }}>
          <View
            style={{
              flex: 0.4,
              alignItems: 'center',
              justifyContent: 'space-evenly',
            }}>
            <View
              style={styles.profileView}>
              {JSON.stringify(dataRecord && dataRecord.avatar) !=
              JSON.stringify({}) ? (
                <Image source={{uri: dataRecord && dataRecord.avatar}} style={styles.profileView}/>
              ) : (
                <Image source={{uri: 'https://indepacific.com/img/user.png'}} style={styles.profileView} />
              )}
            </View>
            <Text>{dataRecord && dataRecord.name}</Text>
            <Image />
          </View>
          <View
            style={{
              flex: 0.2,
              justifyContent: 'space-evenly',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 18,
              }}>{`Email : ${dataRecord && dataRecord.email}`}</Text>
            <Text style={{fontSize: 18}}>{`Phone Number : ${dataRecord &&
              dataRecord.phoneNumber}`}</Text>
          </View>
          <View style={{flex: 0.5, backgroundColor: 'orange'}}>
            <MapView
              //   showsMyLocationButton={true}
              //   showsUserLocation={true}
              style={{
                flex: 1,
              }}
              //   provider={'google'}
              region={{
                latitude: dataRecord && dataRecord.address.region.latitude,
                longitude: dataRecord && dataRecord.address.region.longitude,
                latitudeDelta: 0.1922,
                longitudeDelta: 0.1421,
              }}>
              <Marker
                // draggable
                coordinate={dataRecord && dataRecord.address.region}
                resizeMode={'cover'}
                // onDragEnd={e => this.markerDragHandler(e)}
              >
                {/* <Image source={require('../../../../assets/navigation.png')} style={{ height: 50, width : 50 }}/> */}
              </Marker>
            </MapView>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  fieldView: {height: height / 20, width: width / 1.2, borderBottomWidth: 2},
  profileView: {
    height: width / 3,
    width: width / 3,
    borderRadius: width / 3,
  },
});

export default UserDetailsComp;
