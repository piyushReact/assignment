import React, { Component } from 'react';
import {
  STOP_LOADING,
  START_LOADING,
  addDataDispatch
} from '../../../action';
import { connect, dispatch } from "react-redux";
import SignInComponent from './Container'

const mapStateToProps = state => {
    return {
        LoginResponse: {
            LoginResponseStatus: state.AuthReducer, 
        }
    }
};
const mapDispatchToProps = dispatch => ({
  addDataDispatch: payload => {
    // console.log('SocialLogin  dispatch 2 ==> ', payload);
    dispatch(addDataDispatch(payload));
  },
  START_LOADING: payload => {
    dispatch(START_LOADING());
  },
  STOP_LOADING: () => {
    dispatch(STOP_LOADING());
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(SignInComponent);






