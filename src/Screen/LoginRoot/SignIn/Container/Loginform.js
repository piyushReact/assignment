import React, {Component} from 'react';
import {View,StyleSheet,TouchableOpacity,Dimensions,Text,SafeAreaView,} from 'react-native';
// import LoginForm from '../Component/Loginform';
import {textColor} from '../../../../common/Colors';
import {reduxForm, Field} from 'redux-form';
import MyTextInput from './LoginTextInput';
import validate from './Validation';
import ImagePickComponent from '../../../../components/ImagePickCompo';
import MyCurrentLocation from './MycurrentLocation';
import MyCurrentLocationMap from './MycurrentLocationMap';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const phoneFormatter = number => {
  if (!number) return ''; 
  // NNN-NNN-NNNN 
  const splitter = /.{1,3}/g;
  number = number.substring(0, 10);
  return (
    number
      .substring(0, 7)
      .match(splitter)
      .join('-') + number.substring(7)
  );
};

// remove dashes added by the formatter
const phoneParser = number => (number ? number.replace(/-/g, '') : '');

class SignInComponent extends Component {

    currLocation = async () => {
 await checkContectsPermissions(
 PERMISSIONS.IOS.LOCATION_WHEN_IN_USE, PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION, "Location Permissions", "Please Go to Setting and Provide Location Permissions", "Please Go to Setting and Provide Location Permissions"
 ).then((res) => {console.log(1);
 this.getLocation(); }).catch((err) => {console.log(err) })
 
 }


  render() {
    return (
      <View style={{flex: 1, alignItems: 'center'}}>
        <SafeAreaView />
        <View style={{flex: 1 / 2, justifyContent: 'space-evenly'}}>
          <Field name={'profile'} component={ImagePickComponent} />
          <Text style={{fontSize: 18}}>Form Fillup</Text>
        </View>
        <View style={{flex: 1 / 2}}>
          <View style={styles.fieldView}>
            <Field
              keyboardType="email-address"
              name={'name'}
              component={MyTextInput}
              placeholder={'Enter your name'}
              autoCapitalize={false}
              placeholderTextColor={textColor}
            />
          </View>

          <View style={[styles.fieldView, {marginTop: 20}]}>
            <Field
              keyboardType="email-address"
              name={'email'}
              component={MyTextInput}
              placeholder={'Enter your e-mail'}
              autoCapitalize={false}
              placeholderTextColor={textColor}
            />
          </View>

          <View style={[styles.fieldView, {marginTop: 20}]}>
            <Field
              keyboardType="email-address"
              name={'phone_number'}
              component={MyTextInput}
              //   placeholder={'Enter your phone number'}
              autoCapitalize={false}
              placeholderTextColor={textColor}
              placeholder={'NNN-NNN-NNNN'}
              format={phoneFormatter}
              parse={phoneParser}
            />
          </View>

          <View style={[styles.fieldView, {marginTop: 20}]}>
            <Field              
              name={'current_Location'}
              component={MyCurrentLocation}
            />
          </View>

          <View style={[styles.fieldView, {marginTop: 20}]}>
            <Field
              name={'address'}
              component={MyCurrentLocationMap}
            /> 
          </View>
        </View>

        <View style={{flex: 1 / 1.3, justifyContent: 'space-evenly'}}>
          <TouchableOpacity
            onPress={this.props.handleSubmit}
            style={{
              height: 48,
              justifyContent: 'center',
              alignItems: 'center',
              width: width / 1.6,
              borderWidth: 1,
              borderStyle: 'dashed',
            }}>
            <Text style={{fontSize: 18}}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fieldView: {height: height / 20, width: width / 1.2, borderBottomWidth: 2},
});

export default reduxForm({
  form: 'signIn',
  validate,
})(SignInComponent);
