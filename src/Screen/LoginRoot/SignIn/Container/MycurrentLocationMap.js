import React , { useState } from 'react';
import {TextInput, View, Text, TouchableOpacity, Platform} from 'react-native';
import styles from './TextInput.style';
import {iosApiKey, androidApiKey} from '../../../../common//Constant';
const apiKey = Platform.OS == 'ios' ? iosApiKey : androidApiKey;
import Geocoder from 'react-native-geocoding';
navigator.geolocation = require('@react-native-community/geolocation');
import Geolocation from '@react-native-community/geolocation';
import MapModal from '../../../../components/ModalMap';

export default function MycurrentLocationMap(props) {
     const [visible, isVisible] = useState(false);
     const [region, setRegion] = useState({});
     
  const {input, meta, ...inputProps} = props;
  const validationStyles =
    meta.touched && !meta.active
      ? meta.valid
        ? styles.valid
        : styles.invalid
      : null;
  var hasError = false;
  if (meta.error !== undefined) {
    hasError = true;
  }

  async function getCurrentLocation() {
    await isVisible(!visible)
    findAddress()
  }
  function findAddress(){
      Geolocation.getCurrentPosition(info => {
        Geocoder.init(apiKey);
        const region = {
          latitude: info.coords.latitude,
          longitude: info.coords.longitude,
        };
        setRegion(region);
        // alert(JSON.stringify(region));
        Geocoder.from(info.coords.latitude, info.coords.latitude)
          .then(json => {
            json.results[0].address_components.forEach((value, index) => {
              console.log('result[0]', json.results[0]);
              //  alert(JSON.stringify(json.results[0].formatted_address))
              const finalAddress = json.results[0].formatted_address;
              const setAddress = {
                finalAddress: finalAddress,
                region: region,
              };
              input.onChange(setAddress);
            });
          })
          .catch(error => console.warn(error));
      });
  }
  // function getLocation(){
  //     alert("vbfjk")
  // }

  // if(meta)
  return (
    <View style={[styles.inputContainer, validationStyles]}>
      <TextInput
        style={{width: '100%', height: '100%', justifyContent: 'center'}}
        {...inputProps}
        returnKeyType={props.input.name == 'password' ? 'done' : 'next'}
        onChangeText={input.onChange}
        onBlur={input.onBlur}
        // editable={false}
        // fontFamily= 'Montserrat-Regular'
        onFocus={() => getCurrentLocation()}
        value={input.value.finalAddress}
        style={styles.input}
      />
      {!input.value && (
        <TouchableOpacity
          onPress={() => getCurrentLocation()}
          style={{height: 42, width: '80%'}}>
          <Text style={{fontSize: 16}} numberOfLines={1}>
            CURRENT LOCATION USING MAP
          </Text>
        </TouchableOpacity>
      )}
      {/* { props.input.name == "password" && <TouchableOpacity onPress={()=> props.showAndHIde()} style={{ position : "absolute", right : 5,  padding : 5}}>{props.secureTextEntry ? <Icon name='eye-off'  style={{fontSize: 22, color: colorPrimary}} />:  <Icon name='eye' style={{fontSize: 22, color: colorPrimary}} />}</TouchableOpacity>} */}
      {meta.touched && hasError && (
        <Text
          style={{position: 'absolute', left: 0, bottom: -25, color: 'red'}}>
          {meta.error}
        </Text>
      )}
      {visible && (
        <MapModal
          updateLocation={regn => setRegion(regn)}
          title={'Choose your location'}
          onClose={() => isVisible(false)}
          region={region}
        />
      )}
    </View>
  );
}
