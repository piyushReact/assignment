import React from 'react';
import {TextInput, View, Text, TouchableOpacity, Platform} from 'react-native';
import styles from './TextInput.style';
const apiKey = Platform.OS == 'ios' ? 'AIzaSyB-U-yfQ6l9gW608LPzTXMbb27i80sQz00' : 'AIzaSyD9kdInp8kZ4IoIAF3QLXF_2SKumIchB2Q';
import Geocoder from 'react-native-geocoding';
navigator.geolocation = require('@react-native-community/geolocation');
import Geolocation from '@react-native-community/geolocation';

export default function MycurrentLocation(props) {
  const {input, meta, ...inputProps} = props;
  const validationStyles =
    meta.touched && !meta.active 
      ? meta.valid
        ? styles.valid
        : styles.invalid
      : null;
  var hasError = false;
  if (meta.error !== undefined) {
    hasError = true;
  }
  
async function getCurrentLocation(){
 Geolocation.getCurrentPosition(info => {
//  this.setState({
//  user_lat: info.coords.latitude,
//  user_lng: info.coords.longitude,
//  });
 Geocoder.init(apiKey);
 Geocoder.from(info.coords.latitude, info.coords.longitude)
   .then(json => {
    json.results[0].address_components.forEach((value, index) => {
 console.log('result[0]', json.results[0])
//  alert(JSON.stringify(json.results[0].formatted_address))
  const finalAddress = json.results[0].formatted_address
   input.onChange(finalAddress);

   })
})
   .catch(error => console.warn(error));
 });
}
    
  // if(meta)
  return (
    <View style={[styles.inputContainer, validationStyles]}>
      <TextInput
        style={{width: '100%', height: '100%', justifyContent: 'center'}}
        {...inputProps}
        returnKeyType={props.input.name == 'password' ? 'done' : 'next'}
        onChangeText={input.onChange}
        onBlur={input.onBlur}
        editable={false}
        // fontFamily= 'Montserrat-Regular'
        onFocus={input.onFocus}
        value={input.value}
        style={styles.input}
      />
      {!input.value &&  (
        <TouchableOpacity
          onPress={()=> getCurrentLocation()}
          style={{height: 42, width: '80%'}}>
          <Text style={{fontSize: 16}} numberOfLines={1}>
            CURRENT LOCATION
          </Text>
        </TouchableOpacity>
      )}
      {/* { props.input.name == "password" && <TouchableOpacity onPress={()=> props.showAndHIde()} style={{ position : "absolute", right : 5,  padding : 5}}>{props.secureTextEntry ? <Icon name='eye-off'  style={{fontSize: 22, color: colorPrimary}} />:  <Icon name='eye' style={{fontSize: 22, color: colorPrimary}} />}</TouchableOpacity>} */}
      {meta.touched && hasError && (
        <Text
          style={{position: 'absolute', left: 0, bottom: -25, color: 'red'}}>
          {meta.error}
        </Text>
      )}
    </View>
  );
}
