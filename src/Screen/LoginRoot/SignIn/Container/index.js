import React, { Component } from 'react';
import { View,StyleSheet, Dimensions, Keyboard, Text, Platform, SafeAreaView } from 'react-native';
import * as NavigationService from '../../../../navigation/NavigationService'
import SignComponent from './Loginform';
import Header from '../../../../components/Header'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

 class SignIn extends Component {
  
     submitHandler(data){
        this.props.addDataDispatch(data)
     }

    render() {  
        return (
          <View style={{flex: 1, alignItems: 'center'}}>
            <SafeAreaView />
            <Header
              goBack
              title={'BACK'}
              goBackHandler={() => NavigationService.goBack()}
            />
            <SignComponent onSubmit={data => this.submitHandler(data)} />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    fieldView : { height : height /20, width : width / 1.2 , borderBottomWidth : 2 }
})

export default SignIn;


