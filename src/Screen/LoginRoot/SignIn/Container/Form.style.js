import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { primaryColor, secondaryColor, white, textColor } from '../../../../common/Colors'
import fontsConst from '../../../../common/fontsConst';

export default StyleSheet.create({
  container: {
    padding: 45
  },
  formSubmit: {
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 15,
    marginTop: 25,
    padding: 5
  },
  SignInTxt: { fontFamily: fontsConst.bold_font, fontSize: 50, color: primaryColor, marginTop: hp('3%'), },
  mainView: { width: wp('85%'), height: hp('10%'), flexDirection: 'column', marginTop: hp('2%') },
  emailView: { flex: 1, justifyContent: 'flex-end', paddingLeft: wp('7%') },
  formContainer: { width: '100%', height: '80%', backgroundColor: secondaryColor, borderRadius: 25, alignItems: 'center', justifyContent: 'center', },
  emailIcon: { flex: 1, borderTopLeftRadius: 10, borderBottomLeftRadius: 10, alignItems: 'center', justifyContent: 'center', },
  fieldView: { flex: 6, borderTopRightRadius: 25, borderBottomRightRadius: 25 },
  passwordView: { width: wp('85%'), height: hp('10%'), flexDirection: 'column', marginTop: hp('2%') },
  passFields: { width: '100%', height: '80%', backgroundColor: secondaryColor, borderRadius: 25, alignItems: 'center', justifyContent: 'center', },
  lockImageStyle: { flex: 1, borderTopLeftRadius: 10, borderBottomLeftRadius: 10, alignItems: 'center', justifyContent: 'center', },
  passFieldView: { flex: 6, borderTopRightRadius: 25, borderBottomRightRadius: 25 },
  signInBtnStyle: { width: wp('85%'), height: hp('6.2%'), marginTop: hp('2%'), borderRadius: 25, backgroundColor: primaryColor, justifyContent: 'center', alignItems: 'center', },
  signTex: { fontFamily: fontsConst.bold_font, fontSize: 15, color: white },
  restorebtnStyle: { width: wp('85%'),shadowOffset:{  width: 1,  height: 1,  },
  shadowColor: textColor,
  shadowOpacity: 1.0, height: hp('35%'), marginBottom: hp('15%'), borderRadius: 15, backgroundColor: white, flexDirection: 'column' },
  restoreTextStyle: { fontSize: 20, marginTop: hp('1%'), color: primaryColor, fontFamily: fontsConst.bold_font },

});