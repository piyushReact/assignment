import React from 'react';
import { TextInput, View, Text, TouchableOpacity, Platform } from 'react-native';
import {primaryColor} from '../../../../common/Colors'
import styles from './TextInput.style';
// import { Icon } from 'native-base';
import {colorPrimary } from '../../../../common/Colors'

export default function MyTextInput(props) {

  const { input, meta, ...inputProps } = props; 
  const validationStyles = meta.touched && !meta.active
    ? meta.valid ? styles.valid : styles.invalid
    : null;
    var hasError= false; 
    if(meta.error !== undefined){
      hasError= true;
    }  
    // if(meta)
  return (
    <View style={[styles.inputContainer, validationStyles]}>
    
      <TextInput style={{ width: '100%', height: '100%', justifyContent: 'center', }} 
      {...inputProps}
      returnKeyType={ props.input.name == "password" ? "done" : "next" }
      onChangeText={input.onChange}
      onBlur={input.onBlur}
      // fontFamily= 'Montserrat-Regular'
      onFocus={input.onFocus}
      value={input.value}
      style={styles.input}
      />
      {/* { props.input.name == "password" && <TouchableOpacity onPress={()=> props.showAndHIde()} style={{ position : "absolute", right : 5,  padding : 5}}>{props.secureTextEntry ? <Icon name='eye-off'  style={{fontSize: 22, color: colorPrimary}} />:  <Icon name='eye' style={{fontSize: 22, color: colorPrimary}} />}</TouchableOpacity>} */}
      { meta.touched && hasError && <Text style={{position : "absolute", left : 0, bottom : -25, color : "red" }}>{meta.error}</Text>}
    </View>
  ); 
}