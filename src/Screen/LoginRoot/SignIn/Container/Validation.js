import {emailValidation} from '../../../../common/Utils'

const validate = values => {
  // alert(JSON.stringify(values));
    const error= {};
    error.email= '';
    error.name = "";
    error.phone_number = "";
    error.address = '';
    error.current_Location = '';

    var name = values.name; 
    var ema = values.email;
    var phone_number = values.phone_number;
    var address = values.address;
    var current_Location = values.current_Location;

    
 
    if (values.name === undefined) {
      name = '';
    }
    if(values.email === undefined){
      ema = '';
    }
    if (values.phone_number === undefined) {
      phone_number = '';
    }
    if (values.address === undefined) {
      address = '';
    }
    if (values.current_Location === undefined) {
      current_Location = '';
    }
    
    if(ema.length < 8 && ema !== ''){
      error.forgot_email= 'Too short'; 
    }
    if(!ema.includes('@') && ema !== ''){
      error.email= '@ not included';
    }
    if(values.email !== " " && ema.length > 10 && !emailValidation(values.email)){
      error.email = "Fill correct email"
    }
    
    if (name == '') {
      error.name = 'Fill Your Name';
    }
    if(ema.length == ""){
      error.email= 'Fill email'; 
    }
    if (phone_number == "") {
      error.phone_number = 'Fill Your Phone Number';
    }
    if (address == '') {
      error.address = 'Click On Current Location';
    }
    if (current_Location == '') {
      error.current_Location = 'Click On Current Location Using Map';
    } 
   
  return error;
  };
  
  export default validate;