import { StyleSheet } from 'react-native';
import { colorPrimary } from '../../../../common/Colors'
// import { plaeholderFontStyle } from '../../../../common/CommonStyles'
export default StyleSheet.create({
  input: {
    // padding: 13 , 
    color : colorPrimary,
    fontWeight : "500",
    fontSize : 18

  },
  inputContainer: {
    justifyContent : "center",
    // alignItems : "center",
    marginLeft : 20,
    flex : 1,
  },
  valid: {
    borderColor: '#53E69D'
  },
  invalid: {
    borderColor: '#F55E64'
  }
});