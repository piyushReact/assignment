
import {API_LOGIN , API_REGISTER, API_TERM_CONDI} from './ApiConstant'  
// var END_POINT = "http://192.168.1.34/fishing/index.php/api/"


export const TermAndCondiService = (request) => {
    
    const parameters = { 
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
      // body: JSON.stringify(request.user)
    };
  
    return fetch(API_TERM_CONDI, parameters)
      .then(response => {
          console.log("api call ====> ",response.json())
        return response.json();
      })
      .then(json => {
        return json;
      });
  };
export const registerUserService = (request) => {
    const REGISTER_API_ENDPOINT = API_REGISTER;
    
    const parameters = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(request.user)
    };
  
    return fetch(REGISTER_API_ENDPOINT, parameters)
      .then(response => {
          console.log("response after register api call ====> "+response)
        return response.json();
      })
      .then(json => {
        return json;
      }); 
  };
  
  export const loginUserService = (request) => {
    const LOGIN_API_ENDPOINT = API_LOGIN
  
    const parameters = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(request.user)
    };
  
    return fetch(LOGIN_API_ENDPOINT, parameters)
      .then(response => {
        return response.json();
      })
      .then(json => {
        return json;
      });
  };