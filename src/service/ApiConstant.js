// Local server URL

export const BASE_URL               =   "https://dev.rewardme.net.au/"; 

export const STATUS_SUCCESS         =   200         
// export const STATUS_SUCCESS         =   "success"        
 
export const API_TERM_CONDI               =   BASE_URL + "api/v1/terms/" 




export const API_LOGIN                =   BASE_URL + "login" 
export const API_REGISTER             =   BASE_URL + "register"
export const API_FORGOT_PASS_REQ      =   BASE_URL + "create/password/reset/request"
export const API_CHANGE_PASS          =   BASE_URL + "resetpassword"
export const API_GET_PROFILE          =   BASE_URL + "getprofile"
export const API_UPDATE_PROFILE       =   BASE_URL + "updateprofile" 
export const API_UPDATE_PROFILE_IMAGE =   BASE_URL + "updateprofileimage" 

export const API_SUBMIT_CATCH         =   BASE_URL + "submit_catch"
export const API_SEARCHING            =   BASE_URL + "searchtournament"
export const GET_STATE                =   BASE_URL + "getstate"


export const API_TOKEN_CHECK          =   BASE_URL + "password/find/"
export const API_RESET_PASS           =   BASE_URL + "reset/password"
export const API_LOGOUT               =   BASE_URL + "logout"
export const API_VERIFY_OTP           =   BASE_URL + "verify/phone/number" 
export const API_RESEND_OTP           =   BASE_URL + "resend/otp"
export const API_SKIP_PASS            =   BASE_URL + "skip/forgot/password"
export const API_GET_CATEGORIES       =   BASE_URL + "get/courses/categories"
export const API_GET_POLICIES         =   BASE_URL + "all/policies"
//Home
export const API_ALL_COURSES          =   BASE_URL + "get/all/public/courses"
export const API_ALL_TRAINERS         =   BASE_URL + "get/all/trainers"
// Profile
export const API_VIEW_PROFILE         =   BASE_URL + "view/profile"
export const API_EDIT_PROFILE         =   BASE_URL + "edit/profile"
export const API_DELETE_ACCOUNT       =   BASE_URL + "delete/account"

export const API_CREATE_COURSE        =   BASE_URL + "create/course"


export const api_header = {
    headers: {
        'Content-Type': 'application/json',
    }
}