import React, { Component } from "react";
import { createSwitchNavigator } from "react-navigation";
import LoginRoot from "../navigation/LoginRoot";

export const createRootNavigator = (isLogin) => {
  return createSwitchNavigator(
    {
      LoginRoot: { screen: LoginRoot() },
      // HomeRoot: { screen: LoginRoot() } 
    },
    {
      initialRouteName: "LoginRoot" 
    }
  );
};
 