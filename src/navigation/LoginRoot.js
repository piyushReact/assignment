import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import SignIn from '../Screen/LoginRoot/SignIn';
import ListUser from '../Screen/LoginRoot/ListUser';
import UserDetails from '../Screen/LoginRoot/UserDescription';

// import {navigationConstants} from './NavigationConstant.js';

export default LoginRoot = () => {
    return createStackNavigator(
      {
        ListUser: ListUser,
        SignIn: SignIn,
        UserDetails: UserDetails,
        // [navigationConstants.SignIn]: { screen: SignIn },
      },
      {
        initialRouteName: 'ListUser',
        headerMode: 'none',
      },
    ); 
}