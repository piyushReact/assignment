import { put, call } from 'redux-saga/effects';
import * as types from '../action/Action_Type';
import * as NavigationService from '../navigation/NavigationService'
import { PrintLog } from '../components/PrintLog';
import { getApiWithoutHeader, postApiWithoutHeader, postApi, putApi, patchApi, deleteApi } from '../common/Api'
import {addUser, deleteUser} from '../common/APIConstant';
import { alertMessage, getErrorMessage, isNetworkConnected } from '../common/Utils'


export function* ADD_DESPATCH_SAGA(payload) {
  console.log("payload == > ",payload) 
          let param = {
            "name": payload.payload.name,
            "avatar": payload.payload.profile,
            "email": payload.payload.email,
            "phoneNumber": payload.payload.phone_number,
            "currentLocation": payload.payload.current_Location,
            "address": payload.payload.address,  
          };
          // console
         const response = yield call(postApiWithoutHeader, addUser , param);
         try {
           yield put({ type: types.START_LOADING, response });
         //   PrintLog('checkout saga -=> ', payload);
           if (response.status == '201') {
             yield call(GET_USER_LIST_SAGA, addUser);
             NavigationService.navigate('ListUser');
             yield put({type: types.STOP_LOADING, response});
           } else {
           }
         } catch (e) {
           yield put({type: types.STOP_LOADING});

           console.log('from catch block :==> ', e);
         }
       }

  export function* GET_USER_LIST_SAGA(payload) {
           const response = yield call(getApiWithoutHeader, addUser);
           console.log("checking response ==> ",response)
           try {
             if (response.status == '200') {   
                yield put({type: types.GET_USER_LIST_SAGA_SUCCESS, response});  
                yield put({type: types.STOP_LOADING, response});
             } else { 
                yield put({type: types.GET_USER_LIST_SAGA_FAIL, response});
             }
           } catch (e) {
             yield put({type: types.STOP_LOADING});

             console.log('from catch block :==> ', e);
           }
         }     
          
         //DELETE_USER_SAGA
 
 export function* DELETE_USER_SAGA(payload) {
   const deleteUser = addUser + '/' + payload.payload;
  const response = yield call(deleteApi, deleteUser);
  console.log('DELETE_USER_SAGA ==> ', response);
  // GET_USER_LIST_SAGA;   
  try {
     yield put({ type: types.START_LOADING , response});
     if (response.status == '200') {
     yield call(GET_USER_LIST_SAGA, addUser);
    
       yield put({type: types.STOP_LOADING, response});
     } else {
       yield put({
         type: types.GET_USER_LIST_SAGA_FAIL,
         response,
       });
    yield put({type: types.STOP_LOADING});
     yield put({type: types.TERM_CONDI_SAGA_FAIL, response});
     const message = getErrorMessage(response.data.status);
     setTimeout(() => {
    alertMessage(message);
     }, 300);
     }
  } catch (e) {
    yield put({type: types.STOP_LOADING});

    console.log('from catch block :==> ', e);
  }
}     