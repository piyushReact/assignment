import { takeLatest, put, call } from 'redux-saga/effects';
import {ADD_DESPATCH_SAGA, GET_USER_LIST_SAGA, DELETE_USER_SAGA} from './authsaga';

import * as types from '../action/Action_Type';

export default function* watchUserAuthentication() {

  yield takeLatest(types.ADD_DESPATCH, ADD_DESPATCH_SAGA);
  //GET_USER_LIST
  yield takeLatest(types.GET_USER_LIST, GET_USER_LIST_SAGA);
//DELETE_USER
  yield takeLatest(types.DELETE_USER, DELETE_USER_SAGA);
} 