import * as types from '../../action/Action_Type';

const initialState = {
  data: [],
  dataFetched: false,
  loading: false,
  success: false,
  error: false,
};  

export const userList = (state = initialState, action) => {
      console.log(' userList @piyush ==> ', action.response);

    switch (action.type) {  
      case types.GET_USER_LIST_SAGA_SUCCESS:
        return { 
          ...state,
          data: action.response.data,
          success: true,
          error: action.response.error,
        };
      case types.GET_USER_LIST_SAGA_FAIL:
        return {
          ...state,
          // status: 201,
          // loading: false,
          // data: action.response,
        };
      default:
        return state;
    }
};
export default userList;
