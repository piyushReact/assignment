import * as types from '../action/Action_Type';
// import { PrintLog } from '../components/PrintLog';

const initialState = {
    data: [],
    dataFetched: false,
    loading: false,
    success : false,
    error: false
  } 

export const LoaderReducer = (state = initialState, action) => {
    switch (action.type) {
       
        case types.START_LOADING:
        // PrintLog("start3 reducer")
        return {
            ...state,
           
            loading: true,
            data: action.response
        }
        case types.STOP_LOADING:
        return {
            ...state,
          
            loading: false,
            data: action.response
        }
        default:
            return state;
    }
};
export default LoaderReducer
