import * as types from '../action/Action_Type';

const initialState = {
    data: [],
    dataFetched: false,
    loading: false,
    success : false,
    error: false
  }

export const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.LOGIN_USER_SAGA_SUCCESS:
            return {
                ...state,
                loading: false, 
                data: action.response,
                success : true,
                // error : action.response.error
            }
        case types.LOGIN_USER_SAGA_FAIL:
            return {
                ...state,
                status: 201,
                loading: false,
                data: action.response
            }
        case types.START_LOADING:
            return {
                loading: true,
            }
        case types.STOP_LOADING:
            return {
                loading: false,
            }
        default:
            return state;
    }
};
export default AuthReducer
