import {combineReducers} from 'redux';
import {userList} from './LoginRoot/userList';
import {LoaderReducer} from './LoaderReducer';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
  form: formReducer,
  userList: userList,
  LoaderReducer: LoaderReducer,
});
 