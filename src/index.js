import React, { Component } from 'react'
import { Provider } from 'react-redux'
import RootContainer from './index.router'
import configureStore from './store/configureStore'

const store = configureStore()

export default class Index extends Component{
  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    )
  }
} 
