export const landing_btn_text = "GET STARTED";
export const term_and_condi = "Terms and Conditions";
export const selection_type = "I WANT TO CONTINUE AS:";
export const creatorType = "AN EVENT CREATOR";
export const attendeeType = "AN EVENT ATTENDEE";
export const tandC = "TERMS & CONDITIONS";
export const privacy = "PRIVACY & POLICY";
export const txt_search_event = "SEARCH EVENT"; 

export const fontFamily_Bold = "Montserrat-Bold";
export const fontFamily_Thin = "Montserrat-Thin";
export const fontFamily_Regular = "Montserrat-Regular";

export const txt_take_photo = "Take Photo";
export const txt_choose_gallery = "Choose From Gallery";
export const txt_add_image = "Please Add Image";
export const txt_cancel = "Cancel";

export const txt_dummy = "Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum";

import { Platform, StatusBar, Dimensions } from 'react-native';

export const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
export const iosApiKey = 'AIzaSyB-U-yfQ6l9gW608LPzTXMbb27i80sQz00';
export const androidApiKey = 'AIzaSyD9kdInp8kZ4IoIAF3QLXF_2SKumIchB2Q';

