export const colorPrimary = '#68DBD6';
export const greyText = '#AAB5B5';
export const InputTextBg = '#F1F6F6';

export const primaryColor = '#68DBD6'
export const white = '#FFFFFF'
export const inputBoxBg = '#F4F8F8'

export const secondaryColor = '#F4F8F8'

export const grey = 'grey';
export const textColor = '#AAB5B5';
export const markerColor = '#F53997';
export const textColorLight = '#CBD9D9';
export const turquoise = '#a1ddd1';
export const lightGreen = '#87d37b'; 
export const pink = '#F53997';
export const yellow = '#e9d461';
export const filterBackground = '#f5f5f5';
export const settingBackground = '#494949';
export const notification = '#e44e3f';
export const priceColor = '#9fcda6';

export const black = '#000'; 
export const borderColor = '#C3CBCE';
export const shadowColor = '#67417280';
export const transparent = 'transparent';
export const drawerbackgraoundcolr = "#2f2f2f";