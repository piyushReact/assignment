import React from 'react';
import { AsyncStorage, Platform, Alert, ActionSheetIOS } from 'react-native';
import AStorage from './AsyncStorage';
import { PrintLog } from '../components/PrintLog';
// import NetInfo from '@react-native-community/netinfo'
// import ImagePicker from 'react-native-image-crop-picker';
import { txt_take_photo, txt_choose_gallery, txt_add_image, txt_cancel } from './Constant';

// import ImagePicker from 'react-native-image-crop-picker';

var Utils = {
    emailValidation: (emailId) => { 
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(emailId) !== false) {
            return true;
        }
        return false; 
    },

    
    isNetworkConnected: async () => {
        // await NetInfo.isConnected.fetch().then(isConnected => {
        //     return isConnected
        // });

        // await NetInfo.fetch().then(state => {
        //     return state.isConnected
        //   })
    },


    getErrorMessage : (statusObj) => {
        let errMessage = '';
        if (statusObj) {
            errMessage += statusObj.message;
            let key = statusObj.detail ? Object.keys(statusObj.detail) : []
            if (key.length > 0) {
                for (let i = 0; i < key.length; i++) {
                    errMessage += `\n- ${statusObj.detail[key[i]][0]}`
                }
            }
            statusObj.message = errMessage;
        PrintLog("from utils errMessage==> ",errMessage)
        }
        PrintLog("from utils ==> ",statusObj.message)
        return statusObj.message
    },

    alertMessage: (message) => {
        Alert.alert(
            '', message, [{ text: 'OK', },
            ], { cancelable: false }
        )
    },

    isEmpty: (text) => {
        if (text.toString().trim() === "") {
            return true;
        } else {
            return false;
        }
    },

    apiHeaders: (token) => {
        const apiHeaders = {
            headers: {
                // 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        }
        return apiHeaders
    },
    showPermissionAlert: (title , message) => {
        function sayYes(v){  
           return true
         }
        Alert.alert(
            title,
            message,
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () =>  sayYes("ok")},
            ], {
            cancelable: false
        }
        )
    },
    onChangeNumber: (text) => {
        let newText = ''; 
        let numbers = '0123456789';
        for (var i = 0; i < text.length; i++) {
            if (numbers.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
        }
        return newText
    },

    onChangeCharacter: (text) => {
        let newText = '';
        let ch = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ';
        for (var i = 0; i < text.length; i++) {
            if (ch.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
        }
        return newText
    },

    // showImagePicker: async (index ,circle, type, successPicker) => {
    //     let width = 500, height = 500;
    //     if (type === "profile") {
    //         width = 240;
    //         height = 240;
    //     } else if (type === "background") {
    //         width = 828;
    //         height = 640;
    //     }
    //     let obj = {
    //         width: width,
    //         height: height,
    //         cropping: true,
    //         cropperCircleOverlay: circle,
    //         compressImageMaxWidth: 828,
    //         compressImageMaxHeight: 640,
    //         compressImageQuality: 0.5,
    //         compressVideoPreset: 'MediumQuality',
    //         includeBase64: true,
    //         includeExif: true,
    //     }
    //             if (index == 1) {
    //                 setTimeout(()=>{
    //                     ImagePicker.openPicker(obj).then((image) => {
    //                         let imageData = {
    //                             uri: image.path, width: image.width, height: image.height,
    //                             // name: image.filename === undefined ? "image" : image.filename, 
    //                             name: "eventCover.jpg", 
    //                             type: image.mime
    //                         }
    //                         PrintLog("check this ==> image data ==> abc ",imageData)
    //                         // let base64 = `data:${image.mime};base64,`+ image.data;
    //                         if (image.path) {
    //                             successPicker(image.path, imageData);
    //                         }
    //                     }).catch((response) => { 
    //                         PrintLog("Gallery error : ", response)
    //                     });
    //                 },300)
    //             } 
    //             else {
    //                 setTimeout(()=>{ 
    //                     ImagePicker.openCamera(obj).then((image) => {
    //                         PrintLog("Utils Image : ", image);
    //                         let imageData = {
    //                             uri: image.path, width: image.width, height: image.height,
    //                             // name: image.filename === undefined || image.filename === null ? "image" : image.filename, 
    //                             type: image.mime,
    //                             name: "eventCover.jpg", 
    //                         }
    //                         if (image.path) {
    //                             successPicker(image.path, imageData);
    //                         }
    //                     }).catch((response) => {
    //                         PrintLog("Camera error : ", response)
    //                     });
    //                 },300)
    //             }
        
    // },

    logout: () => {
        AsyncStorage.setItem('isloggedin', JSON.stringify(false));
        AsyncStorage.setItem('tokenAccess', "");
        AsyncStorage.setItem('token', '');
        AsyncStorage.setItem('name', '');
        AsyncStorage.setItem('loginData', '');
        AStorage.flushQuestionKeys();
    }
};

module.exports = Utils;