import React, { Component } from 'react';
import {
    Modal,
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    Image,
    BackHandler,
} from 'react-native';
import { Icon } from 'native-base';

class MultiItemPicker extends Component {
    updateSelectedList = (value) => {
        const { props } = this;
        const selectedItems = [...props.selectedItems];
        const valueIndex = selectedItems.indexOf(value);
        if (props.isSingle) {
            valueIndex !== -1 ? props.onTap([]) : props.onTap([value]);
        } else {
            valueIndex !== -1
                ? selectedItems.splice(selectedItems.indexOf(value), 1)
                : selectedItems.push(value);
            props.onTap(selectedItems);
        }
    };

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.onClose();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    render() {
        const { props, updateSelectedList, icon } = this;
        return (
            <Modal visible={true} animationType={'slide'} requestOnClose={true}>
                <SafeAreaView style={styles.wrapper}>
                    <View style={styles.container}>
                        <View
                            style={[styles.header,{flexDirection : 'row', justifyContent :'space-between',alignItems : 'center',padding : 15}]}>
                            <Text style={[styles.headerText]}>
                                {props.title || 'Select Items'}
                            </Text>
                            {/* <Icon {...cross} onPress={props.onClose} /> */}
                            <TouchableOpacity onPress={props.onClose} style={styles.done}>
                                <Text>Done</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={props.items}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) => {
                                return (
                                    <TouchableOpacity onPress={() => updateSelectedList(item.value)}>
                                        <View style={[styles.item,{flexDirection : 'row', alignItems : 'center'}]}>
                                            {props.selectedItems.includes(item.value) && (
                                                <TouchableOpacity style={{ width: 20, height: 20, overflow: 'hidden',marginRight : 10 }}>
                                                    <Image style={{ height: '100%', width: '100%' ,color : 'red'}} source={require('../../assets/SignUp/done.png')} />
                                                </TouchableOpacity>
                                            )}
                                            <Text style={styles.text}>{item.label}</Text>
                                        </View>
                                    </TouchableOpacity>
                                );
                            }}
                            onEndReached={props.loadMore}
                            onEndReachedThreshold={0.1}
                        />
                    </View>
                </SafeAreaView>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },

    container: {
        padding: 10,
    },
    checked: {
        color: "#68DBD6",
    },
    text: {
        fontSize: 17,
    },
    header: {
        backgroundColor: "#F4F8F8",
    },
    headerText: {
        fontSize: 18,
        textTransform: 'uppercase',
    },
    item: {
        paddingHorizontal: 15,
        borderBottomColor: "#F4F8F8",
        borderBottomWidth: 1,
        minHeight: 55,
    },
    done: {
        padding: 10,
    },
});

export default MultiItemPicker;


/**
 * USAGE :
 *
 * 	<MultiItemPicker
 *    isSingle={true}
      items={this.state.incidentTypes}
      selectedItems={this.state.selectedIncidentTypes}
      onTap={(selectedIncidentTypes) => {
        this.setState({ selectedIncidentTypes });
      }}
      onClose={() => this.setState({ showIncidentTypePicker: false })}
    />
 */