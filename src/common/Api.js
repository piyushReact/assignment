import axios from "axios";
// import axiosRetryInterceptor from "axios-retry-interceptor";
import { PrintLog } from "../components/PrintLog";
const axiosInstance = axios.create();
import AsyncStorage from '../common/AsyncStorage' 


// export const getApi1 = async(urlStr) => {
//   const tokenAccess = await AsyncStorage.read('tokenAccess')
  
//   const header = {   
//     "Accept": "application/json", 
//     "Content-Type": "multipart/form-data",
//     "Authorization": `Bearer ${tokenAccess}`
//   }
//   PrintLog("data we are grting ==> ",tokenAccess) 
//   return axiosInstance
//     .get(urlStr, header)
//     .then(response => {
//       PrintLog("getApi1 api.js ==> ", response)
//       return response; 
//     })
//     .catch(error => {
//       return error;
//     });
// }
export const getApi = async(urlStr) => {
  const tokenAccess = await AsyncStorage.read('tokenAccess')
  
  let apiHeaders = {
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${tokenAccess}`
    }
  };

  // const headers = {   
  //   "Accept": "application/json", 
  //   "Content-Type": "multipart/form-data",
  //   "Authorization": `Bearer ${tokenAccess}`
  // }
  return axiosInstance
    .get(urlStr, apiHeaders)
    .then(response => {
      PrintLog("getApi api.js ==> ", response)
      return response;
    })
    .catch(error => {
      return error;
    });
}
export const postApi = async(urlStr, params) => {
  const tokenAccess = await AsyncStorage.read('tokenAccess')
  
  let apiHeaders = {
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${tokenAccess}`
    }
  };
  return axiosInstance
    .post(urlStr, params, apiHeaders)
    .then(response => {
      PrintLog("postApi api.js ==> ", response)
      return response;
    })
    .catch(error => {
      return error;
    });
}

export const putApi = async(urlStr, params) => {
  const tokenAccess = await AsyncStorage.read('tokenAccess')
  
  let apiHeaders = {
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${tokenAccess}`
    }
  };
  return axiosInstance
    .put(urlStr, params, apiHeaders)
    .then(response => {
      PrintLog("postApi api.js ==> ", response)
      return response;
    })
    .catch(error => {
      return error;
    });
}
export const getApiWithoutHeader = (urlStr) => {
  
  return axiosInstance
    .get(urlStr)
    .then(response => {
      PrintLog("getApiWithoutHeader api.js ==> ", response)
      return response;
    })
    .catch(error => {
      return error;
    });
}
export const postApiWithoutHeader = (urlStr, params) => {
  PrintLog("from api param ==> ", params)
  return axiosInstance
    .post(urlStr, params)
    .then(response => {
      PrintLog("postApiWithoutHeader api.js ==> ", response)
      return response;
    })
    .catch(error => {
      return error;
    });
}
export const patchApi = async(urlStr, params) => {
  const tokenAccess = await AsyncStorage.read('tokenAccess')
  
   
  let apiHeaders = {
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${tokenAccess}`
    }
  };
  return axiosInstance
    .patch(urlStr, params, apiHeaders)
    .then(response => {
      return response;
    })
    .catch(error => {
      return error;
    });
}
export const deleteApi = async(urlStr) => {
  const tokenAccess = await AsyncStorage.read('tokenAccess')
  
  let apiHeaders = {
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${tokenAccess}`
    }
  };
  return axiosInstance
    .delete(urlStr, apiHeaders)
    .then(response => {
      PrintLog("api delete ==> ", response)
      return response;
    })
    .catch(error => {
      PrintLog("api delete error ==> ", error)
      return error;
    });
}