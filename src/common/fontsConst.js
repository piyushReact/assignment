const fontName = 'Montserrat';

export default {                                          // font weight
	thin_font: `${fontName}-Thin`,                       //100
	ultra_light_font: `${fontName}-Ultra Light`,        //200
	light_font: `${fontName}-Light`,                   //300
	reg_font: `${fontName}-Regular`,                  //400
	med_font: `${fontName}-Medium`,                  //500
	semi_bold_font: `${fontName}-SemiBold`,         //600
	bold_font: `${fontName}-Bold`,                 //700
	heavy_font: `${fontName}-Heavy`,              //800
	black_font: `${fontName}-Black`,             //900
};
